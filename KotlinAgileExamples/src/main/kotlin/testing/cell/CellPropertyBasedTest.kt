package testing.cell

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import io.kotlintest.specs.ShouldSpec
import testing.cell.CellStates.Alive
import testing.cell.CellStates.Dead
import io.kotlintest.properties.Gen
import io.kotlintest.properties.forAll

class CellPropertyBasedTest : ShouldSpec() {
    override val oneInstancePerTest = true
    private val neighbours = mutableListOf<GameOfLifeCell>()
    fun populateNeighbours() {
        neighbours.clear()
        for (x in 1..8) {
            neighbours.add(GameOfLifeCell(emptyList()))
        }
    }
    init {
        should("throws when it has too many neighbours") {
            populateNeighbours()
            val exception = shouldThrow<CellStateException> {
                neighbours.add(GameOfLifeCell(emptyList()))
                GameOfLifeCell(neighbours)
            }
        }
        should("be dead by default") {
            populateNeighbours()
            val cell = GameOfLifeCell(neighbours)
            cell.state shouldBe Dead
        }
        should("be able to become alive") {
            populateNeighbours()
            val cell = GameOfLifeCell(neighbours)
            cell.makeAlive()
            cell.state shouldBe Alive
        }
        should("be able to become dead") {
            populateNeighbours()
            val cell = GameOfLifeCell(neighbours)
            cell.makeAlive()
            cell.state shouldBe Alive
            cell.makeDead()
            cell.state shouldBe Dead
        }
        should("become alive when three neighbours are alive") {
            val generator = Gen.choose(0, 8)
            forAll(generator, generator, generator) { x: Int, y: Int, z: Int ->
                if (x != y && x != z && y != z) {
                    populateNeighbours()
                    val cell = GameOfLifeCell(neighbours)
                    neighbours[x].makeAlive()
                    neighbours[y].makeAlive()
                    neighbours[z].makeAlive()
                    cell.changeState()
                    cell.state == Alive
                } else {
                    true
                }
            }
        }
        should("starve when only one neighbour is alive") {
            forAll(Gen.choose(0, 8)) { x: Int ->
                populateNeighbours()
                neighbours[x].makeAlive()
                val cell = GameOfLifeCell(neighbours)
                cell.makeAlive()
                cell.changeState()
                cell.state == Dead
            }
        }
        should("be overcrowded when more than three neighbours are alive") {
            val cell = GameOfLifeCell(neighbours)
            cell.makeAlive()
            neighbours[0].makeAlive()
            neighbours[1].makeAlive()
            neighbours[2].makeAlive()
            neighbours[3].makeAlive()
            cell.changeState()
            cell.state shouldBe Dead
        }
        should("stays dead unless three neighbours are alive") {
            populateNeighbours()
            for (x in 0..8) {
                val cell = GameOfLifeCell(neighbours)
                for (y in 0 until x) {
                    neighbours[y].makeAlive()
                }
                cell.changeState()
                if (x != 3) {
                    cell.state shouldBe Dead
                }
            }
        }
    }
}