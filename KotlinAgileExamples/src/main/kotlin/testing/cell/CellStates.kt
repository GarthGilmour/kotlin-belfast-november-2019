package testing.cell

enum class CellStates {
    Dead, Alive
}