package testing.list

class ListEmptyException(msg: String) : Throwable(msg)