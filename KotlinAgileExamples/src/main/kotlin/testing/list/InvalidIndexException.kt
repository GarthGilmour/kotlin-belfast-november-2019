package testing.list

class InvalidIndexException(msg: String) : Throwable(msg)