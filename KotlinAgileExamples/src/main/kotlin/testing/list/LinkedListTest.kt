package testing.list

import io.kotlintest.TestCaseContext
import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import io.kotlintest.specs.ShouldSpec
import testing.list.InvalidIndexException
import testing.list.ListEmptyException

class LinkedListTest : ShouldSpec() {
    private var list : LinkedList = LinkedList()
    override fun interceptTestCase(context: TestCaseContext, test: () -> Unit) {
        with (context.spec as LinkedListTest) {
            list = LinkedList()
            test()
        }
    }
    fun addFiveItems() {
        list.add("abc")
        list.add("def")
        list.add("ghi")
        list.add("jkl")
        list.add("mno")
    }
    init {
        should("be empty initially") {
            list.empty shouldBe true
            list.size shouldBe 0
        }
        should("have size after content is added") {
            addFiveItems()
            list.empty shouldBe false
            list.size shouldBe 5
        }
        should("be able to retrieve the first item") {
            list.add("abc")
            list.get(0) shouldBe "abc"
        }
        should("be able to retrieve multiple items") {
            addFiveItems()
            list.get(0) shouldBe "abc"
            list.get(1) shouldBe "def"
            list.get(2) shouldBe "ghi"
            list.get(3) shouldBe "jkl"
            list.get(4) shouldBe "mno"
        }
        should("throw when accessing an empty list") {
            val exception = shouldThrow<ListEmptyException> {
                list.get(0)
            }
        }
        should("throw when accessing with negative index") {
            val exception = shouldThrow<InvalidIndexException> {
                addFiveItems()
                list.get(-1)
            }
        }
        should("throw when accessing with invalid index") {
            val exception = shouldThrow<InvalidIndexException> {
                addFiveItems()
                list.get(5)
            }
        }
    }
}