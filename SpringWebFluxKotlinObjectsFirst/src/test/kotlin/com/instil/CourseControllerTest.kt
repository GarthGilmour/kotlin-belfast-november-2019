package com.instil

import com.instil.model.Course
import com.instil.model.CourseDifficulty
import org.junit.Assert.assertEquals
import org.junit.Assert.assertSame
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters.fromObject
import reactor.test.StepVerifier
import java.time.Duration

/*
    Duplication not refactored for demo purposes.
 */

@RunWith(SpringRunner::class)
@SpringBootTest
class CourseControllerTest {
    @Autowired
    private lateinit var context: ApplicationContext
    private lateinit var client: WebTestClient

    @Before
    fun start() {
        client = WebTestClient
                .bindToApplicationContext(context)
                .configureClient()
                .responseTimeout(Duration.ofSeconds(20))
                .build()
    }

    @Test
    fun canFindAllCourses() {
        client.get()
                .uri("/courses")
                .header("Content-Type", MediaType.TEXT_EVENT_STREAM_VALUE)
                .exchange()
                .expectStatus().isOk
                .expectBodyList(Course::class.java)
                .hasSize(12)

    }

    @Test
    fun canFindAllCoursesViaFlux() {
        val result = client.get()
                .uri("/courses")
                .header("Content-Type", MediaType.TEXT_EVENT_STREAM_VALUE)
                .exchange()
                .expectStatus().isOk
                .returnResult(Course::class.java)

        StepVerifier.create(result.responseBody)
                .expectNextMatches { it.id == "AB12"}
                .expectNextMatches { it.id == "CD34"}
                .expectNextMatches { it.id == "EF56"}
                .expectNextCount(9)
                .expectComplete()
                .verify()
    }

    @Test
    fun canFindIndividualCourses() {
        val id = "AB12"
        val result = client.get()
                .uri("/courses/$id")
                .header("Content-Type", MediaType.TEXT_EVENT_STREAM_VALUE)
                .exchange()
                .expectStatus().isOk
                .expectBody(Course::class.java)
                .returnResult()
                .responseBody

        assertEquals("Programming in Scala", result?.title)
        assertSame(CourseDifficulty.BEGINNER, result?.difficulty)
        assertEquals(4, result?.duration)
    }

    @Test
    fun canFindIndividualCoursesViaJson() {
        val id = "AB12"
        client.get()
                .uri("/courses/$id")
                .header("Content-Type", MediaType.TEXT_EVENT_STREAM_VALUE)
                .exchange()
                .expectStatus().isOk
                .expectBody()
                .jsonPath("$.title").isEqualTo("Programming in Scala")
                .jsonPath("$.difficulty").isEqualTo("BEGINNER")
                .jsonPath("$.duration").isEqualTo(4)
    }

    @Test
    @DirtiesContext
    fun canDeleteCourses() {
        val id = "CD34"
        client.delete()
                .uri("/courses/$id")
                .exchange()
                .expectStatus().isOk

        client.get()
                .uri("/courses/$id")
                .header("Content-Type", MediaType.TEXT_EVENT_STREAM_VALUE)
                .exchange()
                .expectStatus().isNotFound
    }

    @Test
    @DirtiesContext
    fun cannotDeleteScalaCourses() {
        val id = "AB12"
        client.delete()
                .uri("/courses/$id")
                .exchange()
                .expectStatus().isBadRequest
    }

    @Test
    @DirtiesContext
    fun canAddCourses() {
        val id = "YZ89"
        val course = Course(id, "Advanced Haskell", CourseDifficulty.ADVANCED, 5)
        client.put()
                .uri("/courses/$id")
                .body(fromObject(course))
                .exchange()
                .expectStatus().isOk

        client.get()
                .uri("/courses/$id")
                .header("Content-Type", MediaType.TEXT_EVENT_STREAM_VALUE)
                .exchange()
                .expectStatus().isOk

        client.get()
                .uri("/courses")
                .header("Content-Type", MediaType.TEXT_EVENT_STREAM_VALUE)
                .exchange()
                .expectStatus().isOk
                .expectBodyList(Course::class.java)
                .hasSize(13)
    }

}
