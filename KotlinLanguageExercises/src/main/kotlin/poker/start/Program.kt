package poker.start

import java.io.File

fun main(args: Array<String>) {
    val file = File("input/pokerHands.txt")
    file.readLines().forEach(::println)
}
