package demos.kotlin.coroutines.launch

import demos.kotlin.coroutines.shared.pingSlowServer
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking<Unit> {
    println("Program starts")

    val jobs = mutableListOf<Job>()
    jobs += launch { println(pingSlowServer(8)) }
    jobs += launch { println(pingSlowServer(6)) }
    jobs += launch { println(pingSlowServer(4)) }
    jobs += launch { println(pingSlowServer(2)) }

    println("All jobs running")
    jobs.forEach { it.join() }
    println("Program complete")
}



