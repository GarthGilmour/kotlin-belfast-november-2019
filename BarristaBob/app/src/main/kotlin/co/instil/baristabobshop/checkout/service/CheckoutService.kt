/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.service

import co.instil.baristabobshop.checkout.model.PaymentMethod
import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drink.persistence.DrinkOrder
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.drinksorder.exceptions.DrinkDoesNotExistInDatabaseException
import kotlinx.coroutines.rx2.rxSingle
import co.instil.baristabobshop.security.JwtService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.rx2.rxCompletable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class CheckoutService @Inject constructor(
    private val checkoutApi: CheckoutApi,
    private val drinkDatastore: DrinkDatastore,
    private val jwtService: JwtService
) {

    private val scope = CoroutineScope(Dispatchers.IO)

    open fun getAllDrinkOrderDetails() = scope.rxSingle {
        drinkDatastore.getAllDrinkOrders()
            .map(::toDrinkOrderDetails)
            .sortedBy { it.drink.name }
    }

    private fun toDrinkOrderDetails(drinkOrder: DrinkOrder): DrinkOrderDetails {
        val drink = drinkDatastore.getDrinkUsingId(drinkOrder.drinkId) ?: throw DrinkDoesNotExistInDatabaseException()
        return DrinkOrderDetails(drinkOrder.quantity, drinkOrder.drinkId, drink)
    }

    open fun sendOrder(drinkOrders: List<DrinkOrderDetails>, paymentMethod: PaymentMethod) = scope.rxCompletable {
        val order = Order(drinkOrders.map { it.drinkId })
        val jwtToken = jwtService.retrieveJwt()
        checkoutApi.checkOut("Bearer $jwtToken", order, paymentMethod.asString).blockingAwait()
    }

    open fun checkoutPendingDrinkOrders() = scope.rxCompletable {
        drinkDatastore.checkoutPendingOrders()
    }
}

data class Order(val items: List<Long>)