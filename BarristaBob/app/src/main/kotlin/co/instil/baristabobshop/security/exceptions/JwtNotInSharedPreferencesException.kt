package co.instil.baristabobshop.security.exceptions

class JwtNotInSharedPreferencesException : RuntimeException()