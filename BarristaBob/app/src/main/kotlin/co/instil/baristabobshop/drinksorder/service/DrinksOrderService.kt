/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.service

import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.drinksorder.exceptions.DrinkDoesNotExistInDatabaseException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.rx2.rxSingle
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class DrinksOrderService @Inject constructor(
    private val drinkDatastore: DrinkDatastore
) {

    val scope = CoroutineScope(Dispatchers.IO)

    open fun getAllDrinksOrderItems() = scope.rxSingle {
        drinkDatastore.getAllDrinkOrders().map {
            val drink = drinkDatastore.getDrinkUsingId(it.drinkId) ?: throw DrinkDoesNotExistInDatabaseException()
            DrinkOrderDetails(it.quantity, it.drinkId, drink)
        }.sortedBy { it.drink.name }
    }
}