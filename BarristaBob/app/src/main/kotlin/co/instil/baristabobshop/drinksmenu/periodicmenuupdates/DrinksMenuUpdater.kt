package co.instil.baristabobshop.drinksmenu.periodicmenuupdates

import androidx.work.ExistingPeriodicWorkPolicy.REPLACE
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit.MINUTES
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DrinksMenuUpdater @Inject constructor(
    private val workManager: WorkManager
) {

    fun schedulePeriodicDrinksMenuUpdate() {
        val workRequest = PeriodicWorkRequestBuilder<DrinksDatabaseUpdateWorker>(15, MINUTES).build()
        workManager.enqueueUniquePeriodicWork("Tag Work", REPLACE, workRequest)
    }
}