package co.instil.baristabobshop.drink.persistence

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import java.util.Date

@Entity(indices = [Index(value = ["name"], unique = true)])
data class Drink(
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "cost") val cost: Double,
    @ColumnInfo(name = "imageId") val imageId: Int,
    @ColumnInfo(name = "timeStamp") val timeStamp: Date
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var drinkId: Long = 0
}