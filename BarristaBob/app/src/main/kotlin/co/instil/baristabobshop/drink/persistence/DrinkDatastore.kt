package co.instil.baristabobshop.drink.persistence

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface DrinkDatastore {

    @Query(
        """
        SELECT * FROM drink
        """
    )
    fun getAllDrinks(): LiveData<List<Drink>>

    @Query(
        """
        SELECT * FROM drink
        ORDER BY timeStamp DESC
        LIMIT 1
        """
    )
    fun getLatestDrink(): Drink

    @Query(
        """
        SELECT COUNT(id) FROM drink
        """
    )
    fun getDrinkCount(): Int

    @Query(
        """
        SELECT * FROM drinkOrder
        WHERE ordered = 0
        """
    )
    fun getAllDrinkOrders(): List<DrinkOrder>

    @Query(
        """
        SELECT * FROM drinkOrder
        WHERE drinkId = :drinkId AND ordered = 0
        LIMIT 1
        """
    )
    fun getDrinkOrderUsingDrinkId(drinkId: Long): DrinkOrder?

    @Query(
        """
        SELECT * FROM drink
        WHERE id = :id
        LIMIT 1
        """
    )
    fun getDrinkUsingId(id: Long): Drink?

    @Query(
        """
        UPDATE drinkOrder
        SET ordered = 1
        WHERE ordered = 0
        """
    )
    fun checkoutPendingOrders()

    @Query(
        """
        DELETE FROM DrinkOrder
        WHERE ordered = 0
        """
    )
    fun clearCurrentDrinkOrder()

    @Query(
        """
        DELETE FROM DrinkOrder
        """
    )
    fun clearAllDrinkOrders()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addListOfDrinks(listOfDrinks: List<Drink>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addDrinkOrder(drinkOrder: DrinkOrder)

    @Delete
    fun deleteDrinkOrder(drinkOrder: DrinkOrder)
}