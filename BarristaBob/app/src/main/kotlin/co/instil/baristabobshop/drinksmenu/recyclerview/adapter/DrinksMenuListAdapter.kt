package co.instil.baristabobshop.drinksmenu.recyclerview.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.R
import co.instil.baristabobshop.databinding.DrinksMenuItemBinding
import co.instil.baristabobshop.drink.persistence.Drink
import co.instil.baristabobshop.drinksmenu.recyclerview.viewmodel.DrinksMenuItemViewModel
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import co.instil.baristabobshop.prompts.ToastPrompt
import co.instil.baristabobshop.scheduler.BaristaBobSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import javax.inject.Inject

class DrinksMenuListAdapter(
    val resourceService: ResourceService
) : RecyclerView.Adapter<DrinksMenuListAdapter.ViewHolder>() {

    @Inject lateinit var drinksMenuService: DrinksMenuService
    @Inject lateinit var baristaBobSchedulers: BaristaBobSchedulers
    @Inject lateinit var toastPrompt: ToastPrompt

    init {
        Application.dependencies?.inject(this)
    }
    private var drinksMenuItemList = listOf<Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(DataBindingUtil.inflate(layoutInflater, R.layout.drinks_menu_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            vm = DrinksMenuItemViewModel(drinksMenuItemList[position], resourceService, drinksMenuService, baristaBobSchedulers, toastPrompt)
        }
    }

    override fun getItemCount() = drinksMenuItemList.size

    fun updateDrinks(drinks: List<Drink>) {
        drinksMenuItemList = drinks
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: DrinksMenuItemBinding) : RecyclerView.ViewHolder(binding.root)
}