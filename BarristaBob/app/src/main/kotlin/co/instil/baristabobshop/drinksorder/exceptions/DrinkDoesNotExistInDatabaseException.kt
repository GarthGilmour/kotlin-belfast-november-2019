/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.exceptions

import java.lang.RuntimeException

class DrinkDoesNotExistInDatabaseException : RuntimeException()