package co.instil.baristabobshop.drinksmenu.activity

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.R
import co.instil.baristabobshop.android.ui.activity.BaseActivity
import co.instil.baristabobshop.databinding.ActivityDrinksMenuBinding
import co.instil.baristabobshop.drinksmenu.recyclerview.adapter.DrinksMenuListAdapter
import co.instil.baristabobshop.drinksmenu.viewmodel.DrinksMenuViewModel
import co.instil.baristabobshop.ui.service.ResourceService
import kotlinx.android.synthetic.main.activity_drinks_menu.drinksMenuRecyclerView
import javax.inject.Inject

open class DrinksMenuActivity : BaseActivity<DrinksMenuViewModel>() {

    @Inject lateinit var drinksMenuViewModel: DrinksMenuViewModel
    @Inject lateinit var resourceService: ResourceService

    private val drinksMenuListAdapter by lazy {
        DrinksMenuListAdapter(resourceService)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Application.dependencies?.inject(this)
        attachViewModel(drinksMenuViewModel)

        val binding = DataBindingUtil.setContentView<ActivityDrinksMenuBinding>(this, R.layout.activity_drinks_menu)
        binding.vm = drinksMenuViewModel

        drinksMenuViewModel.getAllDrinks().observe(this, Observer { drinks ->
            drinksMenuListAdapter.updateDrinks(drinks ?: emptyList())
        })

        configureRecyclerView()
    }

    private fun configureRecyclerView() {
        drinksMenuRecyclerView.apply {
            adapter = drinksMenuListAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
        }
    }
}