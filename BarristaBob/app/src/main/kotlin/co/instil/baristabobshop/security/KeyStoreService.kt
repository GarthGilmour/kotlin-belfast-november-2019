package co.instil.baristabobshop.security

import java.security.KeyStore
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class KeyStoreService @Inject constructor(
    private val keyStore: KeyStore,
    private val keyGeneratorFactory: KeyGeneratorFactory
) {

    fun getOrGenerateSecretKey() =
        if (doesSecretKeyExist()) getSecretKey() else keyGeneratorFactory.generateAndStoreNewKey()

    private fun doesSecretKeyExist() = keyStore.containsAlias("secretKey")

    private fun getSecretKey() = keyStore.getKey("secretKey", null)
}