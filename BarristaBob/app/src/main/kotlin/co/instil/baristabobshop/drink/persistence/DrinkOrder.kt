/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drink.persistence

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import java.util.Date

@Entity(
    foreignKeys = [ForeignKey(entity = Drink::class,
        parentColumns = ["id"],
        childColumns = ["drinkId"])
    ]
)

data class DrinkOrder(
    @ColumnInfo(name = "drinkId") var drinkId: Long,
    @ColumnInfo(name = "quantity") var quantity: Int,
    @ColumnInfo(name = "ordered") var ordered: Boolean,
    @ColumnInfo(name = "timeStamp") var timestamp: Date
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var drinkOrderId: Long = 0

    fun incrementQuantity() = DrinkOrder(drinkId, quantity + 1, false, timestamp)
}