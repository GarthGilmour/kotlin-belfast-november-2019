package co.instil.baristabobshop.login.service

import io.reactivex.Single
import retrofit2.http.Header
import retrofit2.http.POST

interface LoginApi {

    data class LoginResponse(val token: String)

    @POST("login")
    fun login(@Header("Authorization") authorizationHeader: String): Single<LoginResponse>
}