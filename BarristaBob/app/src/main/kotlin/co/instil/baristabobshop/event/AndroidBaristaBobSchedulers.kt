package co.instil.baristabobshop.event

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * This controls the schedulers available for Rx operations.
 * Allows for injection of test schedulers in tests.
 */
@Singleton
open class AndroidBaristaBobSchedulers @Inject constructor() : BaristaBobSchedulers {

    override fun uiScheduler(): Scheduler = AndroidSchedulers.mainThread()

    override fun ioScheduler(): Scheduler = Schedulers.io()
}
