package co.instil.baristabobshop.drinksmenu.service

import co.instil.baristabobshop.drink.persistence.Drink
import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drink.persistence.DrinkOrder
import co.instil.baristabobshop.security.JwtService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.rxCompletable
import java.util.Date
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class DrinksMenuService @Inject constructor(
    private val drinkDatastore: DrinkDatastore,
    private val drinksMenuApi: DrinksMenuApi,
    private val jwtService: JwtService
) {

    val scope = CoroutineScope(Dispatchers.IO)

    open fun updateDrinksMenu() = scope.rxCompletable {
        val accessTokenString = jwtService.retrieveJwt()
        val accessTokenHeader = "Bearer $accessTokenString"
        val lastUpdateForRequest = getTimeStampForRequest()?.time
        val newDrinks = drinksMenuApi.getDrinksMenuUpdate(accessTokenHeader, lastUpdateForRequest).await()
        if (!newDrinks.isEmpty()) {
            addDrinkListToDatabase(newDrinks)
        }
    }

    private fun getTimeStampForRequest() = if (isDrinkDatabaseEmpty()) null else getLatestTimeStamp()

    private fun isDrinkDatabaseEmpty() = drinkDatastore.getDrinkCount() == 0

    open fun getAllDrinks() = drinkDatastore.getAllDrinks()

    private fun getLatestTimeStamp() = drinkDatastore.getLatestDrink().timeStamp

    private fun addDrinkListToDatabase(listOfDrinks: List<Drink>) = drinkDatastore.addListOfDrinks(listOfDrinks)

    open fun incrementDrinkOrderItemQuantity(drinkId: Long) = scope.rxCompletable {
        val drinkOrder = drinkDatastore.getDrinkOrderUsingDrinkId(drinkId)

        if (drinkOrder == null) {
            drinkDatastore.addDrinkOrder(DrinkOrder(drinkId, 1, false, Date()))
        } else {
            drinkDatastore.deleteDrinkOrder(drinkOrder)
            drinkDatastore.addDrinkOrder(drinkOrder.incrementQuantity())
        }
    }
}