package co.instil.baristabobshop.login.service

import co.instil.baristabobshop.api.ApiTest
import com.google.gson.Gson
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import java.net.HttpURLConnection

class LoginApiTest : ApiTest() {

    private lateinit var target: LoginApi

    @Before
    fun beforeEachTest() {
        target = apiServerMock.retrofitFactory.createInstance()
    }

    @Test(expected = HttpException::class)
    fun shouldFailWhenNotAuthorized() {
        enqueueResponse(loginFailureResponse)

        target.login("incorrectUsernamePassword").blockingGet()
    }

    @Test
    fun shouldPassOnSuccessfulLoginResponse() {
        enqueueResponse(loginSuccessResponse)

        target.login("correctUsernamePassword").blockingGet()
    }

    private val loginFailureResponse =
        MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_UNAUTHORIZED)

    private val loginSuccessResponse =
        MockResponse()
            .setBody(Gson().toJson(LoginApi.LoginResponse("my_token")))
            .setResponseCode(HttpURLConnection.HTTP_OK)
}