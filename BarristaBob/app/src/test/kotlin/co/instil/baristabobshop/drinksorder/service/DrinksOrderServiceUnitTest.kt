/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.service

import co.instil.baristabobshop.R
import co.instil.baristabobshop.drink.persistence.Drink
import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drink.persistence.DrinkOrder
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.drinksorder.exceptions.DrinkDoesNotExistInDatabaseException
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import java.util.Date

class DrinksOrderServiceUnitTest {

    private val drinkDataStore = mock<DrinkDatastore>()

    private val drinkOrders = listOf(
        DrinkOrder(1, 5, false, Date()),
        DrinkOrder(2, 1, false, Date()),
        DrinkOrder(3, 3, false, Date())
    )
    private val drinks = listOf(
        Drink("Americano", "description", 1.50, R.drawable.ic_drink_americano, Date()),
        Drink("Latte", "description", 2.00, R.drawable.ic_drink_latte, Date()),
        Drink("Espresso", "description", 1.30, R.drawable.ic_drink_espresso, Date())
    )
    private val drinksOrderItemsSortedAlphabetically = listOf(
        DrinkOrderDetails(5, 1, drinks[0]),
        DrinkOrderDetails(3, 3, drinks[2]),
        DrinkOrderDetails(1, 2, drinks[1])
    )

    private val target = DrinksOrderService(drinkDataStore)

    @Test
    fun shouldGetAllDrinksInOrderAndSortItAlphabetically() {
        givenSuccessfulDatabaseResponses()

        val response = target.getAllDrinksOrderItems().blockingGet()

        assertThat(response, `is`(drinksOrderItemsSortedAlphabetically))
    }

    @Test(expected = DrinkDoesNotExistInDatabaseException::class)
    fun shouldNotReturnAnyDrinksIfTheyDoNotExistInDatabase() {
        givenFailingDatabaseResponse()

        target.getAllDrinksOrderItems().blockingGet()
    }

    private fun givenSuccessfulDatabaseResponses() {
        given(drinkDataStore.getAllDrinkOrders()).willReturn(drinkOrders)
        given(drinkDataStore.getDrinkUsingId(1)).willReturn(drinks[0])
        given(drinkDataStore.getDrinkUsingId(2)).willReturn(drinks[1])
        given(drinkDataStore.getDrinkUsingId(3)).willReturn(drinks[2])
    }

    private fun givenFailingDatabaseResponse() {
        given(drinkDataStore.getAllDrinkOrders()).willReturn(drinkOrders)
        given(drinkDataStore.getDrinkUsingId(1)).willReturn(drinks[0])
        given(drinkDataStore.getDrinkUsingId(2)).willReturn(drinks[1])
        given(drinkDataStore.getDrinkUsingId(3)).willReturn(null)
    }
}