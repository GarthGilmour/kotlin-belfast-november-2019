/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksmenu.recyclerview.viewmodel

import co.instil.baristabobshop.R
import co.instil.baristabobshop.drink.persistence.Drink
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import co.instil.baristabobshop.prompts.ToastPrompt
import co.instil.baristabobshop.scheduler.ImmediateBaristaBobTestSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import java.util.Date

class DrinksMenuItemViewModelUnitTest {

    private val resourceService = mock<ResourceService>()
    private val drinksMenuService = mock<DrinksMenuService>()
    private val baristaBobSchedulers = ImmediateBaristaBobTestSchedulers()
    private val toastPrompt = mock<ToastPrompt>()

    private val drink = Drink("Americano", "description", 1.50, R.drawable.ic_drink_americano, Date())

    val target = DrinksMenuItemViewModel(drink, resourceService, drinksMenuService, baristaBobSchedulers, toastPrompt)

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.format_currency_gbp)).willReturn("£%.2f")
        given(resourceService.getStringResource(R.string.prefix_brewing)).willReturn("Brewing %s")
        given(drinksMenuService.incrementDrinkOrderItemQuantity(drink.drinkId)).willReturn(Completable.complete())
    }

    @Test
    fun shouldAddItemToDatabaseOnClick() = runBlocking<Unit> {
        target.onClickItemBody().join()

        verify(drinksMenuService).incrementDrinkOrderItemQuantity(drink.drinkId)
    }

    @Test
    fun shouldShowToastPromptOnClick() = runBlocking {
        target.onClickItemBody().join()

        verify(toastPrompt).showMessage("Brewing Americano")
    }
}