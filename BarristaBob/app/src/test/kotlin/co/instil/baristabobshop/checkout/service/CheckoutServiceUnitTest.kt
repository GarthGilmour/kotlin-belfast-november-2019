/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.service

import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.checkout.model.PaymentMethod.CASH
import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drink.persistence.DrinkOrder
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.drinksorder.exceptions.DrinkDoesNotExistInDatabaseException
import co.instil.baristabobshop.security.JwtService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import java.util.Date

class CheckoutServiceUnitTest {

    private val checkoutApi = mock<CheckoutApi>()
    private val drinkDatastore = mock<DrinkDatastore>()
    private val jwtService = mock<JwtService>()

    private val drinkA = createDrink(name = "A Drink")
    private val drinkB = createDrink(name = "B Drink")

    private val drinkOrders = listOf(
        DrinkOrder(0, 1, false, Date()),
        DrinkOrder(1, 3, false, Date())
    )
    private val drinkOrderDetailsList = listOf(
        DrinkOrderDetails(1, 0, drinkB),
        DrinkOrderDetails(3, 1, drinkA)
    )

    private val target = CheckoutService(checkoutApi, drinkDatastore, jwtService)

    @Before
    fun beforeEachTest() {
        given(drinkDatastore.getAllDrinkOrders()).willReturn(drinkOrders)
        given(drinkDatastore.getDrinkUsingId(0)).willReturn(drinkB)
        given(drinkDatastore.getDrinkUsingId(1)).willReturn(drinkA)
        given(jwtService.retrieveJwt()).willReturn("token")
        given(checkoutApi.checkOut(any(), any(), any())).willReturn(Completable.complete())
    }

    @Test
    fun shouldReturnCorrectNumberOfDrinksWhenGettingAllDrinkOrderDetails() {
        val drinkOrderDetails = target.getAllDrinkOrderDetails().blockingGet()

        assertThat(drinkOrderDetails.size, `is`(2))
    }

    @Test
    fun shouldSortDrinkOrderDetailsCorrectlyWhenGettingAllDrinkOrderDetails() {
        val drinkOrderDetails = target.getAllDrinkOrderDetails().blockingGet()

        assertThat(drinkOrderDetails[0].drink.name, `is`("A Drink"))
        assertThat(drinkOrderDetails[1].drink.name, `is`("B Drink"))
    }

    @Test
    fun shouldCallCheckoutWithCorrectArguments() {
        val expectedOrder = Order(drinkOrderDetailsList.map { it.drinkId })

        target.sendOrder(drinkOrderDetailsList, CASH).blockingAwait()

        verify(checkoutApi).checkOut("Bearer token", expectedOrder, "Cash")
    }

    @Test
    fun shouldCallDatastoreCheckoutPendingItems() {
        target.checkoutPendingDrinkOrders().blockingAwait()

        verify(drinkDatastore).checkoutPendingOrders()
    }

    @Test(expected = DrinkDoesNotExistInDatabaseException::class)
    fun shouldNotReturnDrinkOrderDetailsIfOneDrinkDoesNotExistInDatabase() {
        given(drinkDatastore.getDrinkUsingId(1)).willReturn(null)

        target.getAllDrinkOrderDetails().blockingGet()
    }
}