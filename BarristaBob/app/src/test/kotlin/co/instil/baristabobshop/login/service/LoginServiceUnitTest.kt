package co.instil.baristabobshop.login.service

import co.instil.baristabobshop.security.JwtService
import co.instil.baristabobshop.services.Base64Service
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class LoginServiceUnitTest {

    private val loginApi = mock<LoginApi>()
    private val base64Service = mock<Base64Service>()
    private val jwtService = mock<JwtService>()

    private val target = LoginService(loginApi, base64Service, jwtService)

    @Test
    fun shouldSetTheAccessTokenOfItselfWhenTheApiReturnsAValidResponse() {
        val username = "instil"
        val password = "password"
        val responseAccessTokenString = "token123"
        val testResponse = Single.just(LoginApi.LoginResponse(responseAccessTokenString))
        given(loginApi.login(any())).willReturn(testResponse)

        target.login(username, password).blockingAwait()

        assertThat(target.accessToken, `is`(responseAccessTokenString))
    }

    @Test
    fun shouldStoreTheJwtTokenWhenLoggedIn() {
        val username = "instil"
        val password = "password"
        val responseAccessTokenString = "token123"
        val testResponse = Single.just(LoginApi.LoginResponse(responseAccessTokenString))
        given(loginApi.login(any())).willReturn(testResponse)

        target.login(username, password).blockingAwait()

        verify(jwtService).storeJwt(responseAccessTokenString)
    }
}