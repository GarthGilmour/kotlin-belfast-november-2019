package co.instil.baristabobshop.drinksmenu.service

import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drink.persistence.DrinkOrder
import co.instil.baristabobshop.security.JwtService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.anyOrNull
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.util.Date

class DrinksMenuServiceUnitTest {

    private val drinksMenuApi = mock<DrinksMenuApi>()
    private val drinkDatastore = mock<DrinkDatastore>()
    private val jwtService = mock<JwtService>()

    private val drink = createDrink()
    private val drinkOrder = DrinkOrder(1, 5, false, Date())

    private val target = DrinksMenuService(drinkDatastore, drinksMenuApi, jwtService)

    @Before
    fun beforeEachTest() {
        given(jwtService.retrieveJwt()).willReturn("JwtToken")
    }

    @Test
    fun shouldRequestLatestDrinkForTimeStampWhenDrinkDatastoreIsNotEmpty() {
        given(drinkDatastore.getDrinkCount()).willReturn(5)
        given(drinkDatastore.getLatestDrink()).willReturn(drink)
        given(drinksMenuApi.getDrinksMenuUpdate(any(), any())).willReturn(Single.just(emptyList()))

        target.updateDrinksMenu().blockingAwait()

        verify(drinkDatastore).getLatestDrink()
    }

    @Test
    fun shouldNotRequestLatestDrinkForTimestampWhenDrinkDatastoreIsEmpty() {
        given(drinkDatastore.getDrinkCount()).willReturn(0)
        given(drinksMenuApi.getDrinksMenuUpdate(any(), anyOrNull())).willReturn(Single.just(emptyList()))

        target.updateDrinksMenu().blockingAwait()

        verify(drinkDatastore, never()).getLatestDrink()
    }

    @Test
    fun shouldAddNewDrinksToDatabaseIfNewDrinksExist() {
        given(drinkDatastore.getDrinkCount()).willReturn(0)
        given(drinksMenuApi.getDrinksMenuUpdate(any(), anyOrNull())).willReturn(Single.just(listOf(drink, drink)))

        target.updateDrinksMenu().blockingAwait()

        verify(drinkDatastore).addListOfDrinks(any())
    }

    @Test
    fun shouldNotAddEmptyDrinksListToDatabase() {
        given(drinkDatastore.getDrinkCount()).willReturn(0)
        given(drinksMenuApi.getDrinksMenuUpdate(any(), anyOrNull())).willReturn(Single.just(emptyList()))

        target.updateDrinksMenu().blockingAwait()

        verify(drinkDatastore, never()).addListOfDrinks(any())
    }

    @Test
    fun shouldIncrementDrinkOrderQuantityIfDrinkExistsInCheck() {
        given(drinkDatastore.getDrinkOrderUsingDrinkId(any())).willReturn(drinkOrder)

        target.incrementDrinkOrderItemQuantity(drinkOrder.drinkId).blockingAwait()

        verify(drinkDatastore).deleteDrinkOrder(any())
        verify(drinkDatastore).addDrinkOrder(any())
    }

    @Test
    fun shouldAddDrinkOrderIfItDoesNotExistInDatabase() {
        given(drinkDatastore.getDrinkOrderUsingDrinkId(any())).willReturn(null)

        target.incrementDrinkOrderItemQuantity(drinkOrder.drinkId).blockingAwait()

        verify(drinkDatastore, never()).deleteDrinkOrder(any())
        verify(drinkDatastore).addDrinkOrder(any())
    }
}