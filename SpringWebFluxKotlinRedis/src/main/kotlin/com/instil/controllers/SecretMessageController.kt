package com.instil.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.time.Duration

@RestController
@RequestMapping("secret")
class SecretMessageController(@Autowired val stringOps: ReactiveRedisOperations<String, String>) {
    private val key = "SecretMessage"

    // Example of getting by key
    @GetMapping
    fun get(): Mono<String> {
        return stringOps.opsForValue().get(key)
    }

    // Example of setting a value
    @PostMapping("/{message}")
    fun set(@PathVariable("message") message: String): Mono<Boolean> {
        return stringOps.opsForValue().set(key, message)
    }

    // Example of a short lived piece of data
    @PostMapping("/short/{message}")
    fun setShortLive(@PathVariable("message") message: String): Mono<Boolean> {
        return stringOps.opsForValue().set(key, message, Duration.ofSeconds(10))
    }

    // Deleting by key
    @DeleteMapping
    fun delete(): Mono<Boolean> {
        return stringOps.opsForValue().delete(key)
    }
}