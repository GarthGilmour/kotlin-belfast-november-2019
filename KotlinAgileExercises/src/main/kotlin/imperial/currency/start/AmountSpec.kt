package imperial.currency.start

import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.ShouldSpec

class AmountSpec : ShouldSpec() {
    override val oneInstancePerTest = true
    private var amount: Amount = Amount(0)

    init {
        should("handle a zero amount") {
            amount.toString() shouldBe "May God bless you"
        }
        should("handle a single penny") {
            amount.add(Unit.PENNY)
            amount.toString() shouldBe "1 penny"
        }
    }
}