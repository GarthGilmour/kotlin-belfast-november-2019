package imperial.currency.finish

class Amount(private var amount: Int) {
    fun add(newAmount: Unit) {
        amount += newAmount.value
    }
    override fun toString(): String {
        return if (amount == 0) "May God bless you" else value()
    }
    private fun value(): String {
        val builder = StringBuilder()
        convert(amount, 0, Unit.values(), builder)
        return builder.toString()
    }
    private fun convert(value: Int,
                        index: Int,
                        units: Array<Unit>,
                        builder: java.lang.StringBuilder) {
        fun chooseLabel(num: Int, unit: Unit) = if (num == 1) unit.singular else unit.plural

        val unit = units[index]
        val times = value / unit.value
        if(times > 0) {
            if (builder.isNotEmpty()) {
                builder.append(" ")
            }
            builder.append(times)
            builder.append(" ")
            builder.append(chooseLabel(times, unit))
        }
        val remainder = value % unit.value
        if (remainder > 0) {
            convert(remainder, index + 1, units, builder)
        }
    }
}