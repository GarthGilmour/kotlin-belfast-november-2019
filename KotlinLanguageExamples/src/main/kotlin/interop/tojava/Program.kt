package interop.tojava

fun main(args: Array<String>) {
    val obj = AwkwardJava()

    val defaultIdea = obj.suggestion
    try {
        defaultIdea.toUpperCase()
    } catch (ex: TypeCastException) {
        println("Whoops...")
    }

    obj.suggestion = "go surfing"
    obj.`object`()

    obj.isAwkward = true
    obj.`object`()
}