package interop.tojava.arrays

fun main(args: Array<String>) {
    val javaUser = JavaArrayUser()
    val kotlinUser = KotlinArrayUser()
    val testInput = arrayOf(Employee(), Employee())

    javaUser.useArray(testInput)
    //kotlinUser.useArray(testInput) //this will not compile
}