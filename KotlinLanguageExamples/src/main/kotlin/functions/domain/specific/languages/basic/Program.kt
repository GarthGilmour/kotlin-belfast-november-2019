package functions.domain.specific.languages.basic

interface Node {
    fun render(indent: String = ""): String
}

open class Element(val tagName: String) : Node {
    val content = mutableListOf<Node>()

    override fun render(indent: String): String {
        val builder = StringBuilder()
        builder.append("$indent<$tagName>\n")
        content.forEach { builder.append(it.render(indent.plus("  "))) }
        builder.append("$indent</$tagName>\n")
        return builder.toString()
    }

    override fun toString(): String {
        return render()
    }
}

class Course : Element("course") {
    fun description(inputFunc: Description.() -> Unit): Unit {
        val description = Description()
        content.add(description)
        description.inputFunc()
    }
    fun modules(inputFunc: ModuleList.() -> Unit): Unit {
        val moduleList = ModuleList()
        content.add(moduleList)
        moduleList.inputFunc()
    }
}

class Description : Element("description") {
    fun text(input: String) {
        content.add(Text(input))
    }
}

class Text(val value: String) : Node {
    override fun render(indent: String): String {
        return "$indent$value\n"
    }
}

class ModuleList : Element("moduleList") {
    fun module(title: String) = content.add(Module(title))

}

class Module(title: String) : Element("module") {
    init {
        content.add(Text(title))
    }
}

fun course(inputFunc: Course.() -> Unit): Course {
    val course = Course()
    course.inputFunc()
    return course
}

fun main(args: Array<String>) {
    val course = course {
        description {
            text("Kotlin Programming")
        }
        modules {
            module("Object Orientation")
            module("Functional Programming")
            module("Java Interoperability")
        }
    }
    println(course)
}