package basics.enums

fun main(args: Array<String>) {
    for (person in CartoonCharacter.values()) {
        println("$person:\t${person.quote}")
    }
    for (direction in Direction.values()) {
        println("---------")
        direction.print()
    }
}